#### CM_KitchenLight circuit:

![View from original circuit](../pic/CM_KitchenLight_original2.png)  

![View from original circuit](../pic/CM_KitchenLight_original1.png)  

![Kicad PCB Viewer for top copper](../pic/CM_Küche_kicad_top_copper.png)  

![Kicad 3D Viewer for the circuit](../pic/CM_Küche.png)  

![Kicad 3D Viewer for top copper](../pic/CM_Küche_top.png)  

![](../pic/CM_KitchenLight_top_copper.png) <br>
For download use this Link to PDF ![](../circuit/CM_KichenLight_top_copper.pdf)

![](../pic/CM_KitchenLight_top_silk.png) <br>
For download use this Link to PDF ![](../circuit/CM_KichenLight_silk.pdf)

![](../pic/CM_KitchenLight_ExtensionBoard_top_copper.png) <br>
For download use this Link to PDF ![](../circuit/CM_KitchenLight_ExtensionBoard_TOP_Copper.pdf)

![](../pic/CM_KitchenLight_ExtensionBoard_top_silk.png) <br>
For download use this Link to PDF ![](../circuit/CM_KitchenLight_ExtensionBoard-SILK.pdf)
