# CMKitchenLight  
Project to upgrade a kitchen toy with microcontolled light.
We have build a micro controlled rgb light.  
In this development we use small (without chip) PLCC-6 rgb leds.  

#### Why?  
If you buy the kitchen toy without lights and if you have a dark or not so
light room then your child have a really dark kitchen.  
Thats is what we want to change!

#### What is needed?
Some toy that will need to be come a microcontolled light.
For all the crircuit components, please look into the circuit directory, we have save the components list for build your own circuit board.

#### This are pictures from kitchen without light control:  
![View from without lights](/pic/CM_KitchenLight_without_light.jpg)    

#### These are pictures from kitchen with light control
![View from with lights](/pic/CM_KitchenLight_with_lights.png) 

#### Some technical parameters
| discription | electrical comment | comment |
| -------- | -------- | -------- |
| atmega 8 | works @16Mhz, 5V, TQFN32, all i/o pins used |  |
| rgb leds | size PLCC-6, @12v | red, blue, green |
| power supply | 300mA@12v needed | Please note that the power jack has a 2.1 diameter in the middle. |

#### Version and History
| version | answer / comment | actual version online |
| -------- | -------- | -------- |
| 0.01 | initialize the firmware |  |
| 1.00 | First release with 4 lights to control and write into local eeprom on microcontoller | online |
| 1.01 | insert a button "down" control to go through the light color | development |
| 1.02 | implement a timer to power off the 4 lights | development |

#### Known issues / Known Requests
| issue / discription | answer / comment | open / close / request | version fixed |
| -------- | -------- | -------- | -------- |
| If i change the color of one light, i can only change the light color in one step. i didn't can go through all lights with hold the button down.  | yes, in the actual version it is very poor, in the next version we change that. | open | 1.01 |
| Sometimes, i changed the color it didn't save the color, because after reset or lost the power then the color is the old values that was defined bevor i changed | Ok, please note that the microcontoller need some times 5 - 10 seconds to write down the new activated values. | closed | 1.00 |
| My power supply has only 150mA@12 volts, can i use that for that project?  | Lets make a calculation -> 4 lights has togehter 12 rgb PLCC-6 leds, all that lights need 30mA. 3 of the lights are serial connected, that means the ampere will not grow up -> that means , 4 x 30mA ~ 120mA + microcontoller ~ 40mA + led ~ 20mA + some safety then we have ~ 200mA, at the beginning, you can read, we need 300mA@12volt power supply. | closed | 1.00 |

#### How can i make the circuits?
* You can do that at home if you want.
* You can use following link https://oshpark.com/uploads/new to lets make the circuit professional
