#ifndef ButtonPress
#define ButtonPress

char Pressed[numberOfButtons];
int Pressed_Confidence_Level[numberOfButtons]; //Measure button press confidence
int Released_Confidence_Level[numberOfButtons]; //Measure button release confidence

char ButtonPressedContinue(int buttonNumber, unsigned char pinOfButton, unsigned char portBit, int confidenceLevel);
char ButtonPressedOneTimes(int buttonNumber, unsigned char pinOfButton, unsigned char portBit, int confidenceLevel);

#endif
