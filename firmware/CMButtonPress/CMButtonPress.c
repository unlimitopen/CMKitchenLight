﻿/*
 * CMButtonPress.c
 
 *  Author: Patrick Hood-Daniel from NEWBIEHACK.com
 */ 

#include <avr/io.h>
#include "../CMGlobal.h"
#include "CMButtonPress.h"

uint8_t success = FALSE;

char ButtonPressedContinue(int buttonNumber, unsigned char pinOfButton, unsigned char portBit, int confidenceLevel)
{
	if (bit_is_clear(pinOfButton, portBit))
	{
		Pressed_Confidence_Level[buttonNumber] ++; //Increase Pressed Confidence
		Released_Confidence_Level[buttonNumber] = 0; //Reset released button confidence since there is a button press
		if (Pressed_Confidence_Level[buttonNumber] > confidenceLevel) //Indicator of good button press
		{
			if (Pressed[buttonNumber] == 0)
			{
				Pressed[buttonNumber] = 1;
				success = 1;
			}
			//Zero it so a new pressed condition can be evaluated
			Pressed_Confidence_Level[buttonNumber] = 0;
		}
	}
	else
	{
		Released_Confidence_Level[buttonNumber] ++; //This works just like the pressed
		Pressed_Confidence_Level[buttonNumber] = 0; //Reset pressed button confidence since the button is released
		
		//success = 0;
		if (Released_Confidence_Level[buttonNumber] > confidenceLevel)
		{
			Pressed[buttonNumber] = 0;
			Released_Confidence_Level[buttonNumber] = 0;
		}
	}
	return success;
}

char ButtonPressedOneTimes(int buttonNumber, unsigned char pinOfButton, unsigned char portBit, int confidenceLevel)
{
	if (bit_is_clear(pinOfButton, portBit))
	{
		Pressed_Confidence_Level[buttonNumber] ++; //Increase Pressed Confidence
		Released_Confidence_Level[buttonNumber] = 0; //Reset released button confidence since there is a button press
		if (Pressed_Confidence_Level[buttonNumber] > confidenceLevel) //Indicator of good button press
		{
			if (Pressed[buttonNumber] == 0)
			{
				Pressed[buttonNumber] = 1;
				success = 1;
			}
			//Zero it so a new pressed condition can be evaluated
			Pressed_Confidence_Level[buttonNumber] = 0;
		}
	}
	else
	{
		Released_Confidence_Level[buttonNumber] ++; //This works just like the pressed
		Pressed_Confidence_Level[buttonNumber] = 0; //Reset pressed button confidence since the button is released
		
		success = 0;
		if (Released_Confidence_Level[buttonNumber] > confidenceLevel)
		{
			Pressed[buttonNumber] = 0;
			Released_Confidence_Level[buttonNumber] = 0;
		}
	}
	return success;
}