/*
 * CMIkeaKitchen.c
 *
 * Created: 09.10.2017 20:35:27
 * Author : Christian Möllers
 *	Please read:
 *	GitHub:		https://github.com/unlimitopen/CMKitchenLight
 */ 

#include <avr/io.h>

#define  F_CPU   8000800
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include "CMGlobal.h"

/**
	@brief	Initialize the rgb led value from internal eeprom
	@param 	none
	@return	none
*/
//
uint8_t EXT1_LED_G_EEPROM EEMEM = 120;
uint8_t EXT1_LED_R_EEPROM EEMEM = 120;
uint8_t EXT1_LED_B_EEPROM EEMEM = 120;
//
uint8_t EXT2_LED_G_EEPROM EEMEM = 120;
uint8_t EXT2_LED_R_EEPROM EEMEM = 120;
uint8_t EXT2_LED_B_EEPROM EEMEM = 120;
//
uint8_t EXT3_LED_G_EEPROM EEMEM = 120;
uint8_t EXT3_LED_R_EEPROM EEMEM = 120;
uint8_t EXT3_LED_B_EEPROM EEMEM = 120;
//
uint8_t EXT4_LED_G_EEPROM EEMEM = 120;
uint8_t EXT4_LED_R_EEPROM EEMEM = 120;
uint8_t EXT4_LED_B_EEPROM EEMEM = 120;
//
volatile static uint16_t EXT1_LED_COUNTER = 0;
volatile static uint16_t EXT2_LED_COUNTER = 0;
volatile static uint16_t EXT3_LED_COUNTER = 0;
volatile static uint16_t EXT4_LED_COUNTER = 0;
//
volatile uint8_t EXT1_LED_B = 0;
volatile uint8_t EXT1_LED_R = 0;
volatile uint8_t EXT1_LED_G = 0;
//
volatile uint8_t EXT1_LED_B_TMP = 0;
volatile uint8_t EXT1_LED_R_TMP = 0;
volatile uint8_t EXT1_LED_G_TMP = 0;
//
volatile uint8_t EXT2_LED_B = 0;
volatile uint8_t EXT2_LED_R = 0;
volatile uint8_t EXT2_LED_G = 0;
//
volatile uint8_t EXT2_LED_B_TMP = 0;
volatile uint8_t EXT2_LED_R_TMP = 0;
volatile uint8_t EXT2_LED_G_TMP = 0;
//
volatile uint8_t EXT3_LED_B = 0;
volatile uint8_t EXT3_LED_R = 0;
volatile uint8_t EXT3_LED_G = 0;
//
volatile uint8_t EXT3_LED_B_TMP = 0;
volatile uint8_t EXT3_LED_R_TMP = 0;
volatile uint8_t EXT3_LED_G_TMP = 0;
//
volatile uint8_t EXT4_LED_B = 0;
volatile uint8_t EXT4_LED_R = 0;
volatile uint8_t EXT4_LED_G = 0;
//
volatile uint8_t EXT4_LED_B_TMP = 0;
volatile uint8_t EXT4_LED_R_TMP = 0;
volatile uint8_t EXT4_LED_G_TMP = 0;
//
uint8_t EXT1_LED_G_Set = 0;
uint8_t EXT1_LED_R_Set = 0;
uint8_t EXT1_LED_B_Set = 0;
//
uint8_t EXT2_LED_G_Set = 0;
uint8_t EXT2_LED_R_Set = 0;
uint8_t EXT2_LED_B_Set = 0;
//
uint8_t EXT3_LED_G_Set = 0;
uint8_t EXT3_LED_R_Set = 0;
uint8_t EXT3_LED_B_Set = 0;
//
uint8_t EXT4_LED_G_Set = 0;
uint8_t EXT4_LED_R_Set = 0;
uint8_t EXT4_LED_B_Set = 0;
//	
uint8_t EXT1_LED_MENU_FLAG = FALSE;
uint8_t EXT2_LED_MENU_FLAG = FALSE;
uint8_t EXT3_LED_MENU_FLAG = FALSE;
uint8_t EXT4_LED_MENU_FLAG = FALSE;

// define a value change flag to wirte the new values into eeprom
uint8_t EXT1_LED_VALUE_CHANGE_FLAG = FALSE;
uint8_t EXT2_LED_VALUE_CHANGE_FLAG = FALSE;
uint8_t EXT3_LED_VALUE_CHANGE_FLAG = FALSE;
uint8_t EXT4_LED_VALUE_CHANGE_FLAG = FALSE;

/**
	@brief	initialisize for mcu
	@param 	none
	@return	none
*/
void makeInit(void)
{
    // TOIE0 for Timer0 
    TIMSK |=  (1<<TOIE0);
	
	// Prescaler for Timer0
	TCCR0 |= (1<<CS00);

	// start value for overflow timer0
	TCNT0 = 0;
	
	BUTTON1_EXT1_PIN_INPUT;
	BUTTON1_EXT1_PIN_HI;
	BUTTON2_EXT1_PIN_INPUT;
	BUTTON2_EXT1_PIN_HI;

	BUTTON1_EXT2_PIN_INPUT;
	BUTTON1_EXT2_PIN_HI;
	BUTTON2_EXT2_PIN_INPUT;
	BUTTON2_EXT2_PIN_HI;

	BUTTON1_EXT3_PIN_INPUT;
	BUTTON1_EXT3_PIN_HI;
	BUTTON2_EXT3_PIN_INPUT;
	BUTTON2_EXT3_PIN_HI;

	BUTTON1_EXT4_PIN_INPUT;
	BUTTON1_EXT4_PIN_HI;
	BUTTON2_EXT4_PIN_INPUT;
	BUTTON2_EXT4_PIN_HI;
	
	LED1_EXT1_PIN_OUTPUT;
	LED1_EXT1_PIN_LOW;
	LED2_EXT1_PIN_OUTPUT;
	LED2_EXT1_PIN_LOW;
	LED3_EXT1_PIN_OUTPUT;
	LED3_EXT1_PIN_LOW;
	
	LED1_EXT2_PIN_OUTPUT;
	LED1_EXT2_PIN_LOW;
	LED2_EXT2_PIN_OUTPUT;
	LED2_EXT2_PIN_LOW;
	LED3_EXT2_PIN_OUTPUT;
	LED3_EXT2_PIN_LOW;

	LED1_EXT3_PIN_OUTPUT;
	LED1_EXT3_PIN_LOW;
	LED2_EXT3_PIN_OUTPUT;
	LED2_EXT3_PIN_LOW;
	LED3_EXT3_PIN_OUTPUT;
	LED3_EXT3_PIN_LOW;

	LED1_EXT4_PIN_OUTPUT;
	LED1_EXT4_PIN_LOW;
	LED2_EXT4_PIN_OUTPUT;
	LED2_EXT4_PIN_LOW;
	LED3_EXT4_PIN_OUTPUT;
	LED3_EXT4_PIN_LOW;
	
	// read the values from eeprom!!
	EXT1_LED_B_TMP = eeprom_read_byte(&EXT1_LED_B_EEPROM);
	EXT1_LED_R_TMP = eeprom_read_byte(&EXT1_LED_R_EEPROM);
	EXT1_LED_G_TMP = eeprom_read_byte(&EXT1_LED_G_EEPROM);
	//
	EXT1_LED_R = EXT1_LED_R_TMP;
	EXT1_LED_G = EXT1_LED_G_TMP;
	EXT1_LED_B = EXT1_LED_B_TMP;
	//
	EXT2_LED_B_TMP = eeprom_read_byte(&EXT2_LED_B_EEPROM);
	EXT2_LED_R_TMP = eeprom_read_byte(&EXT2_LED_R_EEPROM);
	EXT2_LED_G_TMP = eeprom_read_byte(&EXT2_LED_G_EEPROM);
	//
	EXT2_LED_R = EXT2_LED_R_TMP;
	EXT2_LED_G = EXT2_LED_G_TMP;
	EXT2_LED_B = EXT2_LED_B_TMP;
	//
	EXT3_LED_B_TMP = eeprom_read_byte(&EXT3_LED_B_EEPROM);
	EXT3_LED_R_TMP = eeprom_read_byte(&EXT3_LED_R_EEPROM);
	EXT3_LED_G_TMP = eeprom_read_byte(&EXT3_LED_G_EEPROM);
	//
	EXT3_LED_R = EXT3_LED_R_TMP;
	EXT3_LED_G = EXT3_LED_G_TMP;
	EXT3_LED_B = EXT3_LED_B_TMP;
	//
	EXT4_LED_B_TMP = eeprom_read_byte(&EXT4_LED_B_EEPROM);
	EXT4_LED_R_TMP = eeprom_read_byte(&EXT4_LED_R_EEPROM);
	EXT4_LED_G_TMP = eeprom_read_byte(&EXT4_LED_G_EEPROM);
	//
	EXT4_LED_R = EXT4_LED_R_TMP;
	EXT4_LED_G = EXT4_LED_G_TMP;
	EXT4_LED_B = EXT4_LED_B_TMP;
	
	// activate the interrupts
	sei();
}

/**
	@brief	Setup the timer2 overflow, we need this to dim the leds.
	@param 	none
	@return	none
*/
//ISR( TIMER1_COMPA_vect )  // 1ms for manual movement
//{
//	
//}

/**
	@brief	Setup the timer2 overflow, we need this to make pwm for the leds.
	@param 	none
	@return	none
*/
//ISR (TIMER2_OVF_vect)
//{
//	
//}

/**
	@brief	Setup the timer0 overflow, we need this to use pwm for leds
	@param 	none
	@return	none
*/
ISR(TIMER0_OVF_vect) 
{ 
	if ( EXT1_LED_COUNTER >= 255 )
	{
		EXT1_LED_COUNTER = 0;
		if (EXT1_LED_B > 0 )
		{
			LED1_EXT1_PIN_HI;
		}
		else
		{
			LED1_EXT1_PIN_LOW;
		}
		if (EXT1_LED_R > 0 )
		{
			LED2_EXT1_PIN_HI;
		}
		else
		{
			LED2_EXT1_PIN_LOW;
		}

		if (EXT1_LED_G > 0 )
		{
			LED3_EXT1_PIN_HI;
		}
		else
		{
			LED3_EXT1_PIN_LOW;
		}
	}
	else
	{
		if (EXT1_LED_COUNTER >= EXT1_LED_B )
		{
			LED1_EXT1_PIN_LOW;
		}
		
		if (EXT1_LED_COUNTER >= EXT1_LED_R )
		{
			LED2_EXT1_PIN_LOW;
		}

		if (EXT1_LED_COUNTER >= EXT1_LED_G )
		{
			LED3_EXT1_PIN_LOW;
		}
	}
	
////////////////////////
	
	if ( EXT2_LED_COUNTER >= 255 )
	{
		EXT2_LED_COUNTER = 0;
		if (EXT2_LED_B > 0 )
		{
			LED1_EXT2_PIN_HI;
		}
		else
		{
			LED1_EXT2_PIN_LOW;
		}
		if (EXT2_LED_R > 0 )
		{
			LED2_EXT2_PIN_HI;
		}
		else
		{
			LED2_EXT2_PIN_LOW;
		}

		if (EXT2_LED_G > 0 )
		{
			LED3_EXT2_PIN_HI;
		}
		else
		{
			LED3_EXT2_PIN_LOW;
		}
	}
	else
	{
		if (EXT2_LED_COUNTER >= EXT2_LED_B )
		{
			LED1_EXT2_PIN_LOW;
		}
		
		if (EXT2_LED_COUNTER >= EXT2_LED_R )
		{
			LED2_EXT2_PIN_LOW;
		}

		if (EXT2_LED_COUNTER >= EXT2_LED_G )
		{
			LED3_EXT2_PIN_LOW;
		}
	}

////////////////////////

	if ( EXT3_LED_COUNTER >= 255 )
	{
		EXT3_LED_COUNTER = 0;
		if (EXT3_LED_B > 0 )
		{
			LED1_EXT3_PIN_HI;
		}
		else
		{
			LED1_EXT3_PIN_LOW;
		}
		if (EXT3_LED_R > 0 )
		{
			LED2_EXT3_PIN_HI;
		}
		else
		{
			LED2_EXT3_PIN_LOW;
		}

		if (EXT3_LED_G > 0 )
		{
			LED3_EXT3_PIN_HI;
		}
		else
		{
			LED3_EXT3_PIN_LOW;
		}
	}
	else
	{
		if (EXT3_LED_COUNTER >= EXT3_LED_B )
		{
			LED1_EXT3_PIN_LOW;
		}
		
		if (EXT3_LED_COUNTER >= EXT3_LED_R )
		{
			LED2_EXT3_PIN_LOW;
		}

		if (EXT3_LED_COUNTER >= EXT3_LED_G )
		{
			LED3_EXT3_PIN_LOW;
		}
	}

////////////////////////

	if ( EXT4_LED_COUNTER >= 255 )
	{
		EXT4_LED_COUNTER = 0;
		if (EXT4_LED_B > 0 )
		{
			LED1_EXT4_PIN_HI;
		}
		else
		{
			LED1_EXT4_PIN_LOW;
		}
		if (EXT4_LED_R > 0 )
		{
			LED2_EXT4_PIN_HI;
		}
		else
		{
			LED2_EXT4_PIN_LOW;
		}

		if (EXT4_LED_G > 0 )
		{
			LED3_EXT4_PIN_HI;
		}
		else
		{
			LED3_EXT4_PIN_LOW;
		}
	}
	else
	{
		if (EXT4_LED_COUNTER >= EXT4_LED_B )
		{
			LED1_EXT4_PIN_LOW;
		}
		
		if (EXT4_LED_COUNTER >= EXT4_LED_R )
		{
			LED2_EXT4_PIN_LOW;
		}

		if (EXT4_LED_COUNTER >= EXT4_LED_G )
		{
			LED3_EXT4_PIN_LOW;
		}
	}
	
	EXT1_LED_COUNTER++;
	EXT2_LED_COUNTER++;
	EXT3_LED_COUNTER++;
	EXT4_LED_COUNTER++;
}

/**
	@brief	function for pwm the head light led
	@param 	none
	@return	none
*/
void ledButtonSetter(void)
{
	static uint8_t EXT1_LED_MENU = 0;
	static uint8_t EXT2_LED_MENU = 0;
	static uint8_t EXT3_LED_MENU = 0;
	static uint8_t EXT4_LED_MENU = 0;
	
	switch (EXT1_LED_MENU)
	{
		case 0:
		{
			EXT1_LED_R_Set = FALSE;
			EXT1_LED_G_Set = FALSE;
			EXT1_LED_B_Set = FALSE;
			
			if (EXT1_LED_MENU_FLAG == TRUE)
			{
				EXT1_LED_MENU = 1;
			}
			break;
		}
		
		case 1:
		{
			EXT1_LED_G_Set = TRUE;
			EXT1_LED_R_Set = FALSE;
			EXT1_LED_B_Set = FALSE;
			
			if (EXT1_LED_MENU_FLAG == TRUE)
			{
				EXT1_LED_G_Set = FALSE;
				EXT1_LED_MENU = 2;
			}
			break;
		}
		
		case 2:
		{
			EXT1_LED_G_Set = FALSE;
			EXT1_LED_R_Set = TRUE;
			EXT1_LED_B_Set = FALSE;
			
			if ( EXT1_LED_MENU_FLAG == TRUE)
			{
				EXT1_LED_R_Set = FALSE;
				EXT1_LED_MENU = 3;
			}
			break;
		}
		
		case 3:
		{
			EXT1_LED_G_Set = FALSE;
			EXT1_LED_R_Set = FALSE;
			EXT1_LED_B_Set = TRUE;
			
			if ( EXT1_LED_MENU_FLAG == TRUE)
			{
				EXT1_LED_B_Set = FALSE;
				EXT1_LED_MENU = 0;
			}
			break;
		}
	}

////////////////////////

	switch (EXT2_LED_MENU)
	{
		case 0:
		{
			EXT2_LED_R_Set = FALSE;
			EXT2_LED_G_Set = FALSE;
			EXT2_LED_B_Set = FALSE;
			
			if (EXT2_LED_MENU_FLAG == TRUE)
			{
				EXT2_LED_MENU = 1;
			}
			break;
		}
		
		case 1:
		{
			EXT2_LED_G_Set = TRUE;
			EXT2_LED_R_Set = FALSE;
			EXT2_LED_B_Set = FALSE;
			
			if (EXT2_LED_MENU_FLAG == TRUE)
			{
				EXT2_LED_G_Set = FALSE;
				EXT2_LED_MENU = 2;
			}
			break;
		}
		
		case 2:
		{
			EXT2_LED_G_Set = FALSE;
			EXT2_LED_R_Set = TRUE;
			EXT2_LED_B_Set = FALSE;
			
			if ( EXT2_LED_MENU_FLAG == TRUE)
			{
				EXT2_LED_R_Set = FALSE;
				EXT2_LED_MENU = 3;
			}
			break;
		}
		
		case 3:
		{
			EXT2_LED_G_Set = FALSE;
			EXT2_LED_R_Set = FALSE;
			EXT2_LED_B_Set = TRUE;
			
			if ( EXT2_LED_MENU_FLAG == TRUE)
			{
				EXT2_LED_B_Set = FALSE;
				EXT2_LED_MENU = 0;
			}
			break;
		}
	}

////////////////////////

	switch (EXT3_LED_MENU)
	{
		case 0:
		{
			EXT3_LED_R_Set = FALSE;
			EXT3_LED_G_Set = FALSE;
			EXT3_LED_B_Set = FALSE;
			
			if (EXT3_LED_MENU_FLAG == TRUE)
			{
				EXT3_LED_MENU = 1;
			}
			break;
		}
		
		case 1:
		{
			EXT3_LED_G_Set = TRUE;
			EXT3_LED_R_Set = FALSE;
			EXT3_LED_B_Set = FALSE;
			
			if (EXT3_LED_MENU_FLAG == TRUE)
			{
				EXT3_LED_G_Set = FALSE;
				EXT3_LED_MENU = 2;
			}
			break;
		}
		
		case 2:
		{
			EXT3_LED_G_Set = FALSE;
			EXT3_LED_R_Set = TRUE;
			EXT3_LED_B_Set = FALSE;
			
			if ( EXT3_LED_MENU_FLAG == TRUE)
			{
				EXT3_LED_R_Set = FALSE;
				EXT3_LED_MENU = 3;
			}
			break;
		}
		
		case 3:
		{
			EXT3_LED_G_Set = FALSE;
			EXT3_LED_R_Set = FALSE;
			EXT3_LED_B_Set = TRUE;
			
			if ( EXT3_LED_MENU_FLAG == TRUE)
			{
				EXT3_LED_B_Set = FALSE;
				EXT3_LED_MENU = 0;
			}
			break;
		}
	}

////////////////////////

	switch (EXT4_LED_MENU)
	{
		case 0:
		{
			EXT4_LED_R_Set = FALSE;
			EXT4_LED_G_Set = FALSE;
			EXT4_LED_B_Set = FALSE;
			
			if (EXT4_LED_MENU_FLAG == TRUE)
			{
				EXT4_LED_MENU = 1;
			}
			break;
		}
		
		case 1:
		{
			EXT4_LED_G_Set = TRUE;
			EXT4_LED_R_Set = FALSE;
			EXT4_LED_B_Set = FALSE;
			
			if (EXT4_LED_MENU_FLAG == TRUE)
			{
				EXT4_LED_G_Set = FALSE;
				EXT4_LED_MENU = 2;
			}
			break;
		}
		
		case 2:
		{
			EXT4_LED_G_Set = FALSE;
			EXT4_LED_R_Set = TRUE;
			EXT4_LED_B_Set = FALSE;
			
			if ( EXT4_LED_MENU_FLAG == TRUE)
			{
				EXT4_LED_R_Set = FALSE;
				EXT4_LED_MENU = 3;
			}
			break;
		}
		
		case 3:
		{
			EXT4_LED_G_Set = FALSE;
			EXT4_LED_R_Set = FALSE;
			EXT4_LED_B_Set = TRUE;
			
			if ( EXT4_LED_MENU_FLAG == TRUE)
			{
				EXT4_LED_B_Set = FALSE;
				EXT4_LED_MENU = 0;
			}
			break;
		}
	}
}


/**
	@brief	Button Pressed function
	@param 	none
	@return	none
*/
void ButtonisPress(void)
{
    if (ButtonPressedOneTimes(0, PINC, 4, 0))
    {
		EXT1_LED_MENU_FLAG = TRUE;
    }
	else
	{
		EXT1_LED_MENU_FLAG = FALSE;
	}
	
    if (ButtonPressedOneTimes(1, PINC, 5, 0))
    {
		if (EXT1_LED_G_Set == TRUE && EXT1_LED_R_Set == FALSE && EXT1_LED_B_Set == FALSE)
		{
			EXT1_LED_G = EXT1_LED_G + 5;	
		}
		
		if (EXT1_LED_G_Set == FALSE && EXT1_LED_R_Set == TRUE && EXT1_LED_B_Set == FALSE)
		{
			EXT1_LED_R = EXT1_LED_R + 5 ;
		}
		
		if (EXT1_LED_G_Set == FALSE && EXT1_LED_R_Set == FALSE && EXT1_LED_B_Set == TRUE)
		{
			EXT1_LED_B = EXT1_LED_B + 5;
		}
		
		if (EXT1_LED_G_Set == FALSE && EXT1_LED_R_Set == FALSE && EXT1_LED_B_Set == FALSE)
		{
			EXT1_LED_B = EXT1_LED_B + 5;
			EXT1_LED_G = EXT1_LED_G + 5;
			EXT1_LED_R = EXT1_LED_R + 5;
		}

		EXT1_LED_VALUE_CHANGE_FLAG = TRUE;
    }

////////////////////////
	
	if (ButtonPressedOneTimes(2, PIND, 1, 0))
	{
		EXT2_LED_MENU_FLAG = TRUE;
	}
	else
	{
		EXT2_LED_MENU_FLAG = FALSE;
	}
	
	if (ButtonPressedOneTimes(3, PIND, 2, 0))
	{
		if (EXT2_LED_G_Set == TRUE && EXT2_LED_R_Set == FALSE && EXT2_LED_B_Set == FALSE)
		{
			EXT2_LED_G = EXT2_LED_G + 5;
		}
		
		if (EXT2_LED_G_Set == FALSE && EXT2_LED_R_Set == TRUE && EXT2_LED_B_Set == FALSE)
		{
			EXT2_LED_R = EXT2_LED_R + 5;
		}
		
		if (EXT2_LED_G_Set == FALSE && EXT2_LED_R_Set == FALSE && EXT2_LED_B_Set == TRUE)
		{
			EXT2_LED_B = EXT2_LED_B + 5;
		}
		
		if (EXT2_LED_G_Set == FALSE && EXT2_LED_R_Set == FALSE && EXT2_LED_B_Set == FALSE)
		{
			EXT2_LED_B = EXT2_LED_B + 5;
			EXT2_LED_G = EXT2_LED_G + 5;
			EXT2_LED_R = EXT2_LED_R + 5;
		}

		EXT2_LED_VALUE_CHANGE_FLAG = TRUE;
	}
	
////////////////////////
	
	if (ButtonPressedOneTimes(4, PIND, 6, 0))
	{
		EXT3_LED_MENU_FLAG = TRUE;
	}
	else
	{
		EXT3_LED_MENU_FLAG = FALSE;
	}
	
	if (ButtonPressedOneTimes(5, PIND, 7, 0))
	{
		if (EXT3_LED_G_Set == TRUE && EXT3_LED_R_Set == FALSE && EXT3_LED_B_Set == FALSE)
		{
			EXT3_LED_G = EXT3_LED_G + 5;
		}
		
		if (EXT3_LED_G_Set == FALSE && EXT3_LED_R_Set == TRUE && EXT3_LED_B_Set == FALSE)
		{
			EXT3_LED_R = EXT3_LED_R + 5;
		}
		
		if (EXT3_LED_G_Set == FALSE && EXT3_LED_R_Set == FALSE && EXT3_LED_B_Set == TRUE)
		{
			EXT3_LED_B = EXT3_LED_B + 5;
		}
		
		if (EXT3_LED_G_Set == FALSE && EXT3_LED_R_Set == FALSE && EXT3_LED_B_Set == FALSE)
		{
			EXT3_LED_B = EXT3_LED_B + 5;
			EXT3_LED_G = EXT3_LED_G + 5;
			EXT3_LED_R = EXT3_LED_R + 5;
		}

		EXT3_LED_VALUE_CHANGE_FLAG = TRUE;
	}
	
////////////////////////
	
	if (ButtonPressedOneTimes(6, PINB, 3, 0))
	{
		EXT4_LED_MENU_FLAG = TRUE;
	}
	else
	{
		EXT4_LED_MENU_FLAG = FALSE;
	}
	
	if (ButtonPressedOneTimes(7, PINB, 4, 0))
	{
		if (EXT4_LED_G_Set == TRUE && EXT4_LED_R_Set == FALSE && EXT4_LED_B_Set == FALSE)
		{
			EXT4_LED_G = EXT4_LED_G + 5;
		}
		
		if (EXT4_LED_G_Set == FALSE && EXT4_LED_R_Set == TRUE && EXT4_LED_B_Set == FALSE)
		{
			EXT4_LED_R = EXT1_LED_B + 5;
		}
		
		if (EXT4_LED_G_Set == FALSE && EXT4_LED_R_Set == FALSE && EXT4_LED_B_Set == TRUE)
		{
			EXT4_LED_B = EXT4_LED_B + 5;
		}
		
		if (EXT4_LED_G_Set == FALSE && EXT4_LED_R_Set == FALSE && EXT4_LED_B_Set == FALSE)
		{
			EXT4_LED_B = EXT4_LED_B + 5;
			EXT4_LED_G = EXT4_LED_G + 5;
			EXT4_LED_R = EXT4_LED_R + 5;
		}
		
		EXT4_LED_VALUE_CHANGE_FLAG = TRUE;
	}
}

void setValueToEEprom(void)
{
	if (EXT1_LED_VALUE_CHANGE_FLAG == TRUE)
	{	
		uint8_t i = 0;
		EXT1_LED_B_TMP = EXT1_LED_B;
		EXT1_LED_R_TMP = EXT1_LED_R;
		EXT1_LED_G_TMP = EXT1_LED_G;
		
		for (i=0; i<=2; i++)
		{
			updateEEpromByte(&EXT1_LED_G_EEPROM, EXT1_LED_G_TMP);
			updateEEpromByte(&EXT1_LED_R_EEPROM, EXT1_LED_R_TMP);
			updateEEpromByte(&EXT1_LED_B_EEPROM, EXT1_LED_B_TMP);	
		}
		EXT1_LED_VALUE_CHANGE_FLAG = FALSE;
	}
	
	if (EXT2_LED_VALUE_CHANGE_FLAG == TRUE)
	{
		uint8_t i = 0;
		EXT2_LED_B_TMP = EXT2_LED_B;
		EXT2_LED_R_TMP = EXT2_LED_R;
		EXT2_LED_G_TMP = EXT2_LED_G;

		for (i=0; i<=2; i++)
		{
			updateEEpromByte(&EXT2_LED_G_EEPROM, EXT2_LED_G_TMP);
			updateEEpromByte(&EXT2_LED_R_EEPROM, EXT2_LED_R_TMP);
			updateEEpromByte(&EXT2_LED_B_EEPROM, EXT2_LED_B_TMP);
		}
		EXT2_LED_VALUE_CHANGE_FLAG = FALSE;
	}

	if (EXT3_LED_VALUE_CHANGE_FLAG == TRUE)
	{
		uint8_t i = 0;
		EXT3_LED_B_TMP = EXT3_LED_B;
		EXT3_LED_R_TMP = EXT3_LED_R;
		EXT3_LED_G_TMP = EXT3_LED_G;

		for (i=0; i<=2; i++)
		{
			updateEEpromByte(&EXT3_LED_G_EEPROM, EXT3_LED_G_TMP);
			updateEEpromByte(&EXT3_LED_R_EEPROM, EXT3_LED_R_TMP);
			updateEEpromByte(&EXT3_LED_B_EEPROM, EXT3_LED_B_TMP);
		}
		EXT3_LED_VALUE_CHANGE_FLAG = FALSE;
	}
	
	if (EXT4_LED_VALUE_CHANGE_FLAG == TRUE)
	{
		uint8_t i = 0;
		EXT4_LED_B_TMP = EXT4_LED_B;
		EXT4_LED_R_TMP = EXT4_LED_R;
		EXT4_LED_G_TMP = EXT4_LED_G;

		for (i=0; i<=2; i++)
		{
			updateEEpromByte(&EXT4_LED_G_EEPROM, EXT4_LED_G_TMP);
			updateEEpromByte(&EXT4_LED_R_EEPROM, EXT4_LED_R_TMP);
			updateEEpromByte(&EXT4_LED_B_EEPROM, EXT4_LED_B_TMP);
		}
		EXT4_LED_VALUE_CHANGE_FLAG = FALSE;
	}
}

int main(void)
{
	makeInit();
	
	/* Replace with your application code */
	while (1)
	{
		ButtonisPress();
		ledButtonSetter();
		setValueToEEprom();
	}
}


