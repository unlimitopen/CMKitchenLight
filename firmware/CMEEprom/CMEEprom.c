/*
 * CMEEprom.c
 *
 * Created: 18.10.2015 12:33:13
 *   Author : Christian M�llers
 */ 

#include <avr/io.h>
#include "../CMGlobal.h"
#include <avr/eeprom.h>	// EEPROM Zugriffe

#include "CMEEprom.h"

uint8_t EEpromChangingFlag = FALSE;
uint8_t EEpromChangingCount = CounterDef;
uint8_t byteHelpVariable = 0;
uint8_t EEpromStartCount = 0;


uint8_t readEEprom(uint8_t *value)
{
	uint8_t success = FALSE;
	
	eeprom_busy_wait();
	*value = eeprom_read_byte(value);
	
	if (*value != 0xFF)
	{
		success = TRUE;
	}
	
	return success;
}

uint8_t updateEEpromByte(uint8_t *pointer, uint8_t value)
{
	uint8_t success = FALSE;
	
	if ( byteHelpVariable != value && EEpromStartCount != 0 )
	{
			EEpromChangingCount = CounterDef;
			EEpromChangingFlag = TRUE;
	}
	
	if (EEpromChangingFlag == TRUE)
	{
		if (EEpromChangingCount-- <= 1)
		{
			eeprom_update_byte((uint8_t *)pointer, value);
			success = TRUE;
			EEpromChangingFlag = FALSE;
			EEpromChangingCount = CounterDef;
		}
	}
	byteHelpVariable = value;
	EEpromStartCount = 1;
	
	return success;
}

uint8_t updateEEpromWord(uint16_t *pointer, uint16_t value)
{
	uint8_t success = FALSE;
	
	if ( byteHelpVariable != value && EEpromStartCount != 0 )
	{
		EEpromChangingCount = CounterDef;
		EEpromChangingFlag = TRUE;
	}
	
	if (EEpromChangingFlag == TRUE)
	{
		if (EEpromChangingCount-- <= 1)
		{
			eeprom_update_word((uint16_t *)pointer, value);
			success = TRUE;
			EEpromChangingFlag = FALSE;
			EEpromChangingCount = CounterDef;
		}
	}
	byteHelpVariable = value;
	EEpromStartCount = 1;
	
	return success;
}
